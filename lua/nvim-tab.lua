local api = vim.api
local fn = vim.fn

local function get_current_line()
	local bufnr = api.nvim_get_current_buf()
	local row, _ = unpack(api.nvim_win_get_cursor(0))
	return api.nvim_buf_get_lines(bufnr, row - 1, row, false)[1]
end

local function repeat_key(key, num)
	local text = ''
	for _ = 1, num, 1 do
		text = text..key
	end
	return text
end

local function tab()
	local col = fn.col('.') - 1
	local spaces = vim.bo.tabstop
	local line = get_current_line():sub(0, col)

	if col == 0 or line:match '%S' == nil then -- Empty line, insert tab
		return api.nvim_replace_termcodes('<tab>', true, false, true)
	else -- Not empty, insert spaces to where tab would be
		col = fn.virtcol('.') - 1
		col = col % spaces
		spaces = spaces - col
		return repeat_key(' ', spaces)
	end
end

local function backspace(fallback)
	local line = get_current_line()
	local spaces = vim.bo.softtabstop
	if spaces == 0 then spaces = vim.bo.tabstop end

	local _, col = unpack(api.nvim_win_get_cursor(0))

	if line:sub(col, col) == ' ' then
		local virtcol = fn.virtcol('.') - 1
		spaces = virtcol % spaces
		if spaces == 0 then spaces = 4 end
		-- Check each time we're only removing spaces
		local text = ''
		for i = 1, spaces, 1
		do
			local l = col - i + 1
			if line:sub(l, l) == ' ' then
				text = text..'<c-h>'
			end
		end
		return api.nvim_replace_termcodes(text, true, false, true)
	end
	return fallback
end

return {
	tab = tab,
	bs = backspace
}
